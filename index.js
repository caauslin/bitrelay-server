const express = require('express')
const fs = require('fs')
const path = require('path')
const sqlite3 = require('sqlite3').verbose()

const app = express()
const port = process.env.PORT || 3000
const dbPath = process.env.DB_PATH || __dirname + '/data/db.sqlite3'

// initialise
function init() {
  var directoryPath = path.basename(path.dirname(dbPath))
  if (!fs.existsSync(directoryPath)) {
    fs.mkdirSync(directoryPath)
  }

  db = new sqlite3.Database(dbPath)
  db.serialize(() => {
    db.run(`CREATE TABLE IF NOT EXISTS "sms" (
      "id"                INTEGER PRIMARY KEY AUTOINCREMENT,
      "timestamp"         INTEGER,
      "sender"            TEXT,
      "content"           TEXT,
      "time_received"     INTEGER
    )`)
  })
  db.close()
}

init()

app.use(express.json())

app.get('/', (req, res) => {
  res.sendFile(__dirname + '/index.html')
})

app.get('/api/sms', (req, res) => {
  db = new sqlite3.Database(dbPath)
  db.serialize(() => {
    db.all('SELECT * FROM "sms" ORDER BY time_received LIMIT 30', [], (err, rows) => {
      res.setHeader('Content-Type', 'application/json');
      res.end(JSON.stringify(rows));
    })
  })
  db.close()
})

app.post('/api/sms', (req, res) => {
  const timestamp = req.body.timestamp
  const sender = req.body.sender
  const content = req.body.content

  console.log(`Received message from ${sender} (${new Date(timestamp)}): ${content}`)

  db = new sqlite3.Database(dbPath)
  db.serialize(() => {
    const stmt = db.prepare('INSERT INTO "sms" (timestamp, sender, content, time_received) VALUES (?, ?, ?, ?)')
    stmt.run(timestamp, sender, content, new Date().getTime())
    stmt.finalize()
  })
  db.close()

  res.setHeader('Content-Type', 'application/json');
  res.end(JSON.stringify({}));
})

app.listen(port, () => console.log(`Listening on port ${port}!`))
